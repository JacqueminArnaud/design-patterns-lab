package Commande;

import eu.telecomnancy.sensor.TemperatureSensor;

public class DeactivateSensor implements Command {
	
	TemperatureSensor sensor;
		
		public DeactivateSensor(TemperatureSensor sensor){
			this.sensor = sensor;
		}
		

	@Override
	public void execute() {
		sensor.off();
		
	}

}
