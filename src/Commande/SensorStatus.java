package Commande;

import eu.telecomnancy.sensor.TemperatureSensor;

public class SensorStatus implements Command {

TemperatureSensor sensor;
	
	public SensorStatus(TemperatureSensor sensor){
		this.sensor = sensor;
	}
	
	@Override
	public void execute() {
		sensor.getStatus();
		
	}


}
