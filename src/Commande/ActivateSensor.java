package Commande;

import eu.telecomnancy.sensor.TemperatureSensor;

public class ActivateSensor implements Command {

	
	TemperatureSensor sensor;
	
	public ActivateSensor(TemperatureSensor sensor){
		this.sensor = sensor;
	}
	
	@Override
	public void execute() {
		sensor.on();
		
	}

}
