package Commande;

import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.sensor.TemperatureSensor;

public class ValueOfSensor implements Command{

	TemperatureSensor sensor;
	
	public ValueOfSensor(TemperatureSensor sensor){
		this.sensor=sensor;
	}
	
	@Override
	public void execute() {
		try {
			sensor.getValue();
		} catch (SensorNotActivatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
