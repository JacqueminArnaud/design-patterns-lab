package Commande;

import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.sensor.TemperatureSensor;

public class SensorUpdate implements Command{
TemperatureSensor sensor;
	
	public SensorUpdate(TemperatureSensor sensor){
		this.sensor = sensor;
	}
	
	@Override
	public void execute() {
		try {
			sensor.update();
		} catch (SensorNotActivatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


}
