package State;
import java.util.Random;

import eu.telecomnancy.sensor.*;

public class TemperatureSensorActivated implements ISensor{

	
	TemperatureSensor sensor;
	boolean state;
    double value = 0;
	
	public TemperatureSensorActivated(TemperatureSensor sensor){
		this.sensor=sensor;
	}
	
	@Override
	public void on() {
		System.out.println("Already activated");
		
	}

	@Override
	public void off() {
		sensor.setSensorState(sensor.getDeactivaed());
		
	}

	@Override
	public boolean getStatus() {
	return true;
	}

	@Override
	public void update() throws SensorNotActivatedException {
        if (state)
            value = (new Random()).nextDouble() * 100;
        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        if (state)
            return value;
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }

}
