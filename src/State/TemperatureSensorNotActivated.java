package State;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.sensor.TemperatureSensor;

public class TemperatureSensorNotActivated implements ISensor{

	
	TemperatureSensor sensor;
	
	
	public TemperatureSensorNotActivated(TemperatureSensor sensor){
		this.sensor=sensor;
	}
	
	
	@Override
	public void on() {
		sensor.setSensorState(sensor.getActivated());
		
	}

	@Override
	public void off() {
		System.out.println("ALready deactivated");
		
	}

	@Override
	public boolean getStatus() {
		
		return false;
	}

	@Override
	public void update() throws SensorNotActivatedException {
		System.out.println("Sensor Not Activated");
		
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		System.out.println("Sensor Not Activated");
		return 0;
	}

}
