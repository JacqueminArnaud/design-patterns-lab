package Observer;

public interface Observable {
	
	public void notifyObservers();

	public void addObserver(Observer o);
	
	public void deleteObserver(Observer o);
}
