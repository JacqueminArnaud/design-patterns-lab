package eu.telecomnancy.sensor;

import java.util.ArrayList;
import java.util.Random;

import Observer.AbstractSensor;
import Observer.Observer;

public class TemperatureSensor extends AbstractSensor implements ISensor {
	
	//Observer Design Pattern
	public ArrayList<Observer> observers;
	
	//State Design Pattern
	ISensor TemperatureSensorActivated;
	ISensor TemperatureSensorNotActivated;
	ISensor sensor;
	
	//Normal Implementation
    boolean state;
    double value = 0;
    
    public TemperatureSensor(){
    	observers=new ArrayList<Observer>();
    	
    	TemperatureSensorActivated = new State.TemperatureSensorActivated(this);
    	TemperatureSensorNotActivated = new State.TemperatureSensorNotActivated(this);
    	sensor = TemperatureSensorNotActivated;
    }
    
    //State  Implementation
    
    public void setSensorState(ISensor sensor){
    	this.sensor = sensor;
    }
    
    public void on(){
    	sensor.on();
    }
    
    public void off(){
    	sensor.on();
    }
    
    public ISensor getActivated(){
    	return TemperatureSensorActivated;
    }
    
    public ISensor getDeactivaed(){
    	return TemperatureSensorNotActivated;
    }
    
    
/*//Normal implementation
    @Override
    public void on() {
        state = true;
    }

    @Override
    public void off() {
        state = false;
    }
*/
    @Override
    public boolean getStatus() {
        return state;
    }

    @Override
    public void update() throws SensorNotActivatedException {
        if (state)
            value = (new Random()).nextDouble() * 100;
        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        if (state)
            return value;
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }

	@Override
	public void notifyObservers() {
		for(Observer o : observers){
			o.update();
		}
		
	}

	@Override
	public void addObserver(Observer o) {
		observers.add(o);
		
	}

	@Override
	public void deleteObserver(Observer o) {
		int index=observers.indexOf(o);
		observers.remove(index);
		
	}

}
