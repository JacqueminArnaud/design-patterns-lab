package Decorator;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public abstract class SensorDecorator implements ISensor {
	
	protected ISensor sensor;
	
	public SensorDecorator(ISensor sensor){
		this.sensor=sensor;
		
	}
	
	public void on(){
		sensor.on();
	}
	
	public void off(){
		sensor.off();
	}
	
	public  double getValue(){
		try {
			return sensor.getValue();
		} catch (SensorNotActivatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
	}
	

}
