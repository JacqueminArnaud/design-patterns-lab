package Decorator;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class TemperatureSensorFarenheit extends SensorDecorator{

	public TemperatureSensorFarenheit(ISensor sensor) {
		super(sensor);
		
	}

	@Override
	public void on() {
		sensor.on();
		
	}

	@Override
	public void off() {
		sensor.off();
		
	}

	@Override
	public double getValue() {
		try {
			return sensor.getValue()*9/5+32;
		} catch (SensorNotActivatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
		
	}

	@Override
	public boolean getStatus() {
		return sensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		sensor.update();
		
	}

}
