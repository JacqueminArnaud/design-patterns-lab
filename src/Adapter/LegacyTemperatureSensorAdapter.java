package Adapter;

import java.util.Random;

import eu.telecomnancy.sensor.*;

public class LegacyTemperatureSensorAdapter implements ISensor{

	LegacyTemperatureSensor sensor;
	
	public LegacyTemperatureSensorAdapter(LegacyTemperatureSensor newSensor){
		sensor=newSensor;
	}
	@Override
	public void on() {
		sensor.onOff();
		
	}

	@Override
	public void off() {
		sensor.onOff();
		
	}

	@Override
	public boolean getStatus() {
		return sensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		sensor.getTemperature();
		
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return sensor.getTemperature();
	}

}
